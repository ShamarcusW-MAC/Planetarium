//
//  PlanetariumApp.swift
//  Planetarium
//
//  Created by Sha'Marcus Walker on 8/17/23.
//

import SwiftUI

@main
struct PlanetariumApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView(planetsViewModel: PlanetsViewModel(manager: NetworkManager()))
        }
    }
}
