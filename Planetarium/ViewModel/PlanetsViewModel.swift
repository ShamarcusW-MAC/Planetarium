//
//  PlanetsViewModel.swift
//  Planetarium
//
//  Created by Sha'Marcus Walker on 8/17/23.
//

import Foundation
import Combine


class PlanetsViewModel: ObservableObject {
    let manager: NetworkProtocol
        
    @Published var planetList = [Planet]()
        
    private var subscriptions = Set<AnyCancellable>()

    init(manager: NetworkProtocol) {
        self.manager = manager
    }
    
    deinit {
        subscriptions.forEach { cancellable in
            cancellable.cancel()
        }
    }
    
    func fetchPlanetsList() {
        
        let cancellable = manager.fetchData(endpoint: Endpoints.fetchPlanets())
            .receive(on: RunLoop.main)
            .sink { [weak self] completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print("Error fetching data: \(error)")
                }
                
            } receiveValue: { [weak self] system in
                print("Planets: \(system.results)")
                self?.planetList = system.results
            }

        subscriptions.insert(cancellable)
        
    }
    
}
