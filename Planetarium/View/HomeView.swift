//
//  ContentView.swift
//  Planetarium
//
//  Created by Sha'Marcus Walker on 8/17/23.
//

import SwiftUI

struct HomeView: View {
    
    @ObservedObject var planetsViewModel: PlanetsViewModel

    var body: some View {
        
        GeometryReader { planetGeometry in
            VStack {
                                
                List {
                    ForEach(planetsViewModel.planetList, id: \.self) { planet in
                            
                            Text(planet.name ?? "UnNamed Planet")
                            .frame(width: planetGeometry.size.width, height: planetGeometry.size.height / 10)
                                .foregroundColor(.black)
                                .background(.blue)
                                .font(.system(size: 24))

                    }
                    
                }
                .onAppear {
                    planetsViewModel.fetchPlanetsList()
                }
            }
            .padding()
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView(planetsViewModel: PlanetsViewModel(manager: NetworkManager()))
    }
}
